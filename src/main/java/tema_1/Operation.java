package tema_1;


import java.util.ArrayList;
import java.util.List;

public class Operation {
	
	public static Polinom add(Polinom p1, Polinom p2) {
		Polinom rez = new Polinom();
		rez.addAllMonom(p1.getMonom());
		rez.addAllMonom(p2.getMonom());
		
		rez= rez.removeGradeEq();
		rez= rez.removeCzero();
		
		return rez;
		
		
	}
	
	public static Polinom sub(Polinom p1, Polinom p2) {
		Polinom rez = new Polinom();

		for (Monom m : p2.getMonom()) {
			m.setCoef((-1) * m.getCoef());
		}

		rez = add(p1, p2);

		return rez;
	}

	public static Polinom mul(Polinom p1, Polinom p2) {
		Polinom rez = new Polinom();

		for (Monom m1 : p1.getMonom()) {
			for (Monom m2 : p2.getMonom()) {
				rez.addMonom(m1.multiply(m2));
			}
		}

		rez= rez.removeGradeEq();
		rez= rez.removeCzero();

		return rez;
	}
	public static Polinom derive(Polinom p) {
		List<Monom> rem = new ArrayList<Monom>();
		
		for (Monom m : p.getMonom()) {
			double coeff = m.getCoef();
			int grade = m.getPow();
			
			if (grade == 0) {
				rem.add(m);
			}
			
			m.setCoef(coeff * grade);
			m.setPow(grade - 1);
		}
		
		p.removeAllMonom(rem);
		
		p = p.removeGradeEq();
		p= p.removeCzero();
		return p;
	}
	
	public static Polinom integrate(Polinom p) {
		//for-each
		for (Monom m : p.getMonom()) {
			m.setCoef(m.getCoef() / (m.getPow() + 1));
			m.setPow(m.getPow() + 1);
		}

		p= p.removeGradeEq();
		p= p.removeCzero();
		
		return p;
	}


}
