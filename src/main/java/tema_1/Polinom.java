package tema_1;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Polinom {
	
	private List<Monom> monom= new ArrayList<Monom>();

	//constructor ce primeste un vector de polinoame
	public Polinom(List<Monom> monom) {
		this.monom= monom;
	}
	
	public Polinom() {
	}
	
	//constructor ce primeste un string
	//stringul va fi transformat in vector de monoane
	public Polinom(String s) {
		monom= validarePolinom(s).monom;
	}
	
	
	
	public Polinom multiplyMonom(Monom m) {
		Polinom poly = new Polinom();
		
		for (Monom m2 : monom) {
			poly.addMonom(m2.multiply(m));
		}
		
		return poly;
	}
	
	
	public void addMonom(Monom m) {
		monom.add(m);
	}
	
	public void addAllMonom(List<Monom> m) {
		monom.addAll(m);
	}
	
	public void removeMonom(Monom m) {
		monom.remove(m);
		
	}
	
	public void removeAllMonom(List<Monom> m) {
		monom.removeAll(m);
	}
	
	public List<Monom> getMonom(){
		return monom;
	}
	
	
	//retunr monumul cu indexul primit ca parametru
	public Monom getMonom(int index) {
		return monom.get(index);
	}
	
	//no of monoms from polynom
	public int dim() {
		return monom.size();
	}
	
	//se parcurge lista de monoame,si aduna monoame cu aceeasi putere
	public Polinom removeGradeEq() {
		Polinom pol = new Polinom(monom);
		List<Monom> remove = new ArrayList<Monom>();
		
		for (int i = 0; i < pol.dim() - 1; i++) {
			for (int j = i + 1; j < pol.dim(); j++) {
				if (pol.getMonom(i).getPow() == pol.getMonom(j).getPow()) {
					double nCoef = pol.getMonom(i).getCoef() + pol.getMonom(j).getCoef();
					pol.getMonom(i).setCoef(nCoef);
					remove.add(pol.getMonom(j));
				}
			}
		}
		
		pol.removeAllMonom(remove);
		return pol;
	}
	
	//se verifica daca polinomul primit ca parametru este egal cu polinomul actual
	public boolean equals(Polinom p) {
		Collections.sort(monom);
		Collections.sort(p.monom);
		
		if (dim() != p.dim()) {
			return false;
		}
		
		for (int i = 0; i < dim(); i++) {
			if (!getMonom(i).equals(p.getMonom(i))) {
				return false;
			}
		}
		
		return true;
	}
	
	//converteste un string primit ca argument intr-un polinom
	private static Polinom validarePolinom(String polyString) {
		
		Polinom poly= new Polinom();

	
		
		polyString = polyString.replace("-", "+-");
		polyString = polyString.replace("X", "x");

		for (String monom: polyString.split("\\+")) {

			if (monom.isEmpty()) {
				continue;
			}

			double coef;
			int pow;

			if (!monom.contains("x")) {
				coef = Float.parseFloat(monom);
				pow = 0;
				poly.addMonom(new Monom(coef, pow));
				continue;
			}

			if (!monom.contains("^")) {
				pow = 1;
				if (monom.charAt(0) == 'x') {
					coef = 1;
				} else if (monom.charAt(0) == '-' && monom.charAt(1) == 'x') {
					coef = -1;
				} else {
					monom = monom.replace("x", "");
					coef = Float.parseFloat(monom);
				}
				poly.addMonom(new Monom(coef, pow));
				continue;
			}

			if (monom.contains("x^")) {
				if (monom.charAt(0) == 'x') {
					monom = monom.replace("x^", "");
					coef = 1;
					pow = Integer.parseInt(monom);
				} else if (monom.charAt(0) == '-' && monom.charAt(1) == 'x') {
					monom = monom.replace("-x^", "");
					coef = -1;
					pow = Integer.parseInt(monom);
				} else {
					String[] buff = monom.split("x\\^");
					coef = Float.parseFloat(buff[0]);
					pow = Integer.parseInt(buff[1]);
				}
				poly.addMonom(new Monom(coef, pow));
			}
		}
		return poly;
	}
	
	//elimina monoame cu coef=0
	public Polinom removeCzero() {
		Polinom pol = new Polinom(monom);
		List<Monom> rem = new ArrayList<Monom>();
		//for-each
		for (Monom m : pol.monom) {
			if (m.getCoef() == 0) {
				rem.add(m);
			}
		}
		
		pol.removeAllMonom(rem);
		return pol;
	}
	
	//suprascrie metoda din Object
	//se foloseste de metoda toString din Monom
	@Override
	public String toString() {
		Collections.sort(monom);
		String polString = "";
		
		for (Monom m : monom) {
			polString += m;
		}
		
		return polString;
	}
	
	}

