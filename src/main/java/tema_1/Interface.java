package tema_1;

import java.awt.event.*;
import javax.swing.*;

public class Interface extends JFrame{
	
	private static final long serialVersionUID = 1L;
	
	public JPanel panel= new JPanel();
	public JLabel polinomLabel= new JLabel("Introduceti polinoamele ");
	public JLabel opLabel= new JLabel("Alegeti optiunea ");
	public JLabel pol1Label= new JLabel("Polinom 1 =");
	public JLabel pol2Label= new JLabel("Polinom 2 =");
	public JLabel resultLabel= new JLabel("Rezultat");
	
	public JTextField pol1TF= new JTextField();
	public JTextField pol2TF= new JTextField();
	public JTextField resultTF= new JTextField();
	
	
	public JButton addButton= new JButton("Aduna");
	public JButton subButton= new JButton("Scade");
	public JButton mulButton= new JButton("Inmulteste");
	public JButton divButton= new JButton("Imparte");
	public JButton intButton= new JButton("Integrare");
	public JButton derButton= new JButton("Derivare");
	
	
	public Interface() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(800,600);
		setTitle("Procesarea polinoamelor");
		
		add(panel);
		panel.setLayout(null);
		
		polinomLabel.setBounds(30,100, 500, 25);
		panel.add(polinomLabel);
		
		pol1Label.setBounds(50,150,120,25);
		panel.add(pol1Label);
		
		pol2Label.setBounds(50,180,120,25);
		panel.add(pol2Label);
		
		pol1TF.setBounds(200, 150, 500,25);
		panel.add(pol1TF);
		
		pol2TF.setBounds(200,180,500,25);
		panel.add(pol2TF);
		
		opLabel.setBounds(30,250,500,25);
		panel.add(opLabel);
		
		addButton.setBounds(50, 300, 100,40);
		panel.add(addButton);
		
		subButton.setBounds(170, 300, 100, 40);
		panel.add(subButton);

		mulButton.setBounds(290, 300, 100, 40);
		panel.add(mulButton);

		divButton.setBounds(410, 300, 100, 40);
		panel.add(divButton);

		derButton.setBounds(530, 300, 100, 40);
		panel.add(derButton);

		intButton.setBounds(650, 300, 100, 40);
		panel.add(intButton);

		resultLabel.setBounds(50, 400, 130, 25);
		panel.add(resultLabel);
		
		resultTF.setBounds(200, 400, 550, 25);
		resultTF.setEditable(false);
		panel.add(resultTF);



		addButton.addActionListener(new ActionListener() {
		
			public void actionPerformed(ActionEvent e) {
				try {
					Polinom p1 = new Polinom(pol1TF.getText());
					Polinom p2 = new Polinom(pol2TF.getText());
					resultTF.setText(Operation.add(p1, p2).toString());
				} catch (Exception exc) {
					JOptionPane.showMessageDialog(null, "Read error", "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		subButton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				try {
					Polinom p1 = new Polinom(pol1TF.getText());
					Polinom p2 = new Polinom(pol2TF.getText());
					resultTF.setText(Operation.sub(p1, p2).toString());
				
				} catch (Exception exc) {
					JOptionPane.showMessageDialog(null, "Read error", "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		
		mulButton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				try {
					Polinom p1 = new Polinom(pol1TF.getText());
					Polinom p2 = new Polinom(pol2TF.getText());
					resultTF.setText(Operation.mul(p1, p2).toString());
					
				} catch (Exception exc) {
					JOptionPane.showMessageDialog(null, "Read error", "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		derButton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				try {
					Polinom p = new Polinom(pol1TF.getText());
					resultTF.setText(Operation.derive(p).toString());
			
				} catch (Exception exc) {
					JOptionPane.showMessageDialog(null, "Read error", "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		intButton.addActionListener(new ActionListener() {
		
			public void actionPerformed(ActionEvent e) {
				try {
					Polinom p = new Polinom(pol1TF.getText());
					resultTF.setText(Operation.integrate(p).toString() + "+C");
				
				} catch (Exception exc) {
					JOptionPane.showMessageDialog(null, "Read error", "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		setResizable(false);
		setLocationRelativeTo(null);
		
		setVisible(true);
		
	}
	


}
