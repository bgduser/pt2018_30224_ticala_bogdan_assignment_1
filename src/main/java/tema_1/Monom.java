package tema_1;

import java.util.Comparator;


public class Monom implements Comparable<Monom> {
	
	
	public int pow;
	public double coef;
	
//constructor care retine puterea si coeficientul
	public Monom(double coef, int pow) {
		this.pow=pow;
		this.coef=coef;
	}
	
//returneaza coeficientul
	public double getCoef() {
		return this.coef;
		
	}
//seteaza coeficientul
	public void setCoef (double coef) {
		this.coef= coef;
	}
	
//returneaza puterea
	public int getPow() {
		return this.pow;
	}
//seteaza puterea
	public void setPow(int pow) {
		this.pow=pow;
	}
//inmultirea a 2 monoame, retuneaza tot un monom
	public Monom multiply(Monom multy) {
		return new Monom(coef * multy.coef, pow + multy.pow);
	}
	
//verifica daca 2 monoame sunt egale
	public boolean equals(Monom m) {
		if (pow == m.pow && coef == m.coef) {
			return true;
		}
		return false;
	}
	
	
	public void addCoef(int x) {
		this.coef=this.coef+x;
		
	}
	

	
	public static Comparator<Monom> getOrderPow() {   
		Comparator<Monom> cmp = new Comparator<Monom>(){
		   
		    public int compare(Monom s1, Monom s2) {
		    	return Integer.compare(s1.pow, s2.pow);
		    }        
		};
		return cmp;
	}  
//se suprascrie metoda toString din clasa Object(Comparable)
//retuneaza un sir pentru a reprezenta monoame
	@Override
	public String toString() {
		if (coef == (int) coef) {
			if (coef > 0) {
				if (pow == 0) {
					return "+" + (int) coef;
				} else {
					return "+" + (int) coef + "x^" + pow;
				}
			} else {
				if (pow == 0) {
					return (int) coef + "";
				} else {
					return (int) coef + "x^" + pow;
				}
			}
		} else {
			if (coef > 0) {
				if (pow == 0) {
					return "+" + String.format("%.2f", coef);
				} else {
					return "+" + String.format("%.2f", coef) + "x^" + pow;
				}
			} else {
				if (pow == 0) {
					return String.format("%.2f", coef) + "";
				} else {
					return String.format("%.2f", coef) + "x^" + pow;
				}
			}
		}
	}
	
//ordonare descrescatoare. clasa Monom implementeaza Comparable.
//se suprascrie metoda compareTo din clasa Object

	public int compareTo(Monom o) {
		return (new Integer(o.pow)).compareTo(new Integer(pow));
	}

	
}


	

