package tema_1;

import static org.junit.Assert.*;

import org.junit.Test;

public class PolinomTest {
	private Polinom p1 = new Polinom("5x^2+2");
	private Polinom p2 = new Polinom("-2x+1");
	
	@Test
	public void testAdd() {
		Polinom rez = Operation.add(p1, p2);
		assertTrue(rez.equals(new Polinom("+5x^2-2x^1+3")));
	}
	
	@Test
	public void testSub() {
		Polinom rez = Operation.sub(p1, p2);
		assertTrue(rez.equals(new Polinom("+5x^2+2x^1+1")));
	}
	
	@Test
	public void testMul() {
		Polinom rez = Operation.mul(p1, p2);
		assertTrue(rez.equals(new Polinom("-10x^3+5x^2-4x+2")));
	}
	
	@Test
	public void testDerivare() {
		Polinom rez = Operation.derive(p1);
		assertTrue(rez.equals(new Polinom("+10x")));
	}
	
	@Test
	public void testIntegrare() {
		Polinom rez = Operation.integrate(p2);
		assertTrue(rez.equals(new Polinom("-x^2+x")));
	}
	
	
}
